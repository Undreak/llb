import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.ndimage import gaussian_filter1d
from scipy.signal import find_peaks
from skimage.io import imread
import re
import os
from scipy.optimize import curve_fit
from scipy.signal import savgol_filter
import matplotlib.cm as cm
import csv

def time_step(text):
    """
    Calculate the time between two images from a timelapse

    text (str): text of the properties (.xml) file

    return: time_passed time between two images
    """
    time_pattern = r' Time="(\d{1,2}:\d{2}:\d{2} [AP]M)'
    matches = re.findall(time_pattern, text)
    
    start_time=matches[0]
    end_time=matches[1]

    start_hour = int(start_time.split(' ')[0].split(':')[0])

    # Convert PM/AM
    if start_time.split(' ')[1] == 'PM':
        if start_hour == 12:
            pass
        else: start_hour += 12

    elif start_time.split(' ')[1] == 'AM':
        if start_hour == 12:
            start_hour = 0
        else:
            start_hour

    start_minute = int(start_time.split(' ')[0].split(':')[1])
    start_second = int(start_time.split(' ')[0].split(':')[2])

    end_hour = int(end_time.split(' ')[0].split(':')[0])
    # Convert PM/AM
    if end_time.split(' ')[1] == 'PM':
        if end_hour == 12:
            pass
        else: end_hour += 12
    elif end_time.split(' ')[1] == 'AM':
        if end_hour == 12:
            end_hour = 0
        else:
            end_hour
    end_minute = int(end_time.split(' ')[0].split(':')[1])
    end_second = int(end_time.split(' ')[0].split(':')[2])
    #print(end_hour, ':', end_minute, ':', end_second)
    
    start_time_seconds = start_hour * 60 * 60 + start_minute * 60
    end_time_seconds = end_hour * 60 * 60 + end_minute * 60
    time_passed = end_time_seconds - start_time_seconds
    return time_passed

def h(peaks):
    """
    Calculate the height of the droplet from its interference fringes.

    peaks (int): position of the fringes.

    return: h (µm) height of the droplet
    """
    h = np.zeros(len(peaks))
    lmbda = 488/1000
    # La LED est centrée sur 475nm, mais le filtre est à 488nm

    n = 1.4
    # indice 1.4 https://www.sigmaaldrich.com/FR/fr/product/aldrich/378399
    for i in range(len(peaks) - 2, -1, -1):
        h[i] = h[i+1] + lmbda/(2*n)
    return h
    
def spherical_cap_volume(r, h):
    """
    Calculate the volume of a spherical cap.

    r (float): radius of the sphere.
    h (flaot): height of the cap.

    return: V (µL^3) volume of the spherical cap
    """
    return ((1/6) * np.pi * h * (3*r**2 + h**2))*1e-9


def power_law(t, a, a0):
        return a0 * (t ** a)

def power_law_fit(r,t, a=0.1, a0=100):
    '''
    Calculate a fit of the radius over time
    
    r (lists, float): radius data of timelapses
    t (lists, float): times data of timelapses

    return: r_fit (lists, float): all the radius in one single list
            t_fit (lists, float): all the times in one single list
            popt (float): fitted power indice
    '''
    try:
        r_tot = [item for sublist in r for item in sublist]
    except:
        r_tot = r
    try:
        t_tot = [item for sublist in t for item in sublist]
    except:
        t_tot = t
    try: 
        popt, pcov = curve_fit(power_law, t_tot, r_tot, p0 = (a, a0), maxfev=1000000)
        r_fitted = power_law(t_tot, *popt)
    except:
        r_fitted = 0
        popt = [0,0]
    return r_fitted, t_tot, popt

def radius_film(average, x_dist, r_ini=0, prominence=0.02, limit=0.0005, sigma=30):
    '''
    Calculate the radius of the film from the profile data and time

    t (lists, float): times data of timelapses
    average (lists, float): profile intensity data of timelapses
    r_ini (float): initial radius

    return: r (lists, float): radius of the film
    '''
    r_film = [[] for num in range(len(x_dist))]   
    for i, coord in enumerate(x_dist):
        coord = coord + r_ini
        pct_b = 30
        pct_f = 2
        lin_bg = np.polyfit(coord[-int(len(coord)*pct_b/100):], average[i][0][-int(len(coord)*pct_b/100):],1)
        for data in average[i]:
            try:
                peaks, _ = find_peaks(data, prominence=np.std(data), width=0.1)
                peaks = outliers(peaks)
                dr = peaks[-1]+(peaks[-1]-peaks[-2])//2
                z = data - (lin_bg[0]*coord+lin_bg[1])
                int_f = z[dr:]
                int_f = int_f/np.min(int_f)
                sdf = savgol_filter(int_f, 30, 1)
                bf = np.argwhere(sdf<pct_f/100)[0]
                r_film[i].append(coord[dr:][bf][0])
            except: 
                r_film[i].append(0)
                
    return r_film

def timelapse(path, region_switch=False):
    '''
    Import the timelapse images and extract the intensity profile, distance and time.

    path (string): path of the folder containing the timelapse
    region_switch (bool): orientation of the image in a timelapse, 
                        False if the droplet edge is on the right (default), 
                        True if the droplet edge is on the left. 

    return: average (list, float) intensity profile of the different timelapse
            x_dist (list, float) distance data
            dt (float) time step between images
            delay (float) time between different timelapse
    '''
    folder_path = f'../{path}'
    # find the timelapse folders
    filenames = [file for file in os.listdir(folder_path) if file.startswith('timelapse')]

    # find the number of timelapse folders
    num_timelapse = int(filenames[-1][-1])

    # set up an empty list for storing images
    images=[[] for num in range(num_timelapse)]
    
    # extract the images from a timelapse folder
    for i, file in enumerate(filenames):
        image_path = f'{folder_path}/{file}/'
        images[i] = [file for file in os.listdir(image_path)]
        for j in range(len(images[i])):
            images[i][j] = imread(os.path.join(image_path, images[i][j]), as_gray=True)
            if region_switch:
                images[i][j] = images[i][j][:,::-1] 
                #In this code, `X[::-1]` means "start at the end of the list and end at position 0, move with the step -1".
    
    # initialization: number of images, lenght of the images, position of the stage.
    frame_count = np.zeros(num_timelapse, dtype=np.int8)
    length_x_vec = np.zeros(num_timelapse)
    pos_x = np.zeros(num_timelapse)

    x_dist = [[] for num in range(num_timelapse)]
    y_dist = [[] for num in range(num_timelapse)]
    dt = np.zeros(num_timelapse)
    delay = np.zeros(num_timelapse)
    diff = 0
    for i, file in enumerate(filenames):
        image_path = f'{folder_path}/{file}/'
        image_name = os.listdir(image_path)[0].split('_t')[0]
        metadata = f'../{path}/MetaData/{image_name}_Properties.xml'
        with open(metadata, 'r') as file:
            text = file.read()
        frame_count[i] = re.search(r'<FrameCount>(\d+) \(\d+ channels, \d+ frames\)</FrameCount>', text).group(1)
        length_x = float(re.search(r'DimID="X".*?Length="([^"]*)"' , text).group(1).replace(',', ''))
        length_x_vec[i] = length_x
        length_y = float(re.search(r'DimID="Y".*?Length="([^"]*)"' , text).group(1).replace(',', ''))
        x_dist[i] = np.linspace(0, length_x, images[i][0].shape[1])
        y_dist[i] = np.linspace(0, length_y, images[i][0].shape[0])
        ############################################################################################################
        start_time = re.search(r'<StartTime>(\d{1,2}/\d{1,2}/\d{4} \d{1,2}:\d{2}:\d{2}\.\d+ [AP]M)</StartTime>',
                      text).group(1)
        start_hour = int(start_time.split(' ')[1].split(':')[0])
        if start_time.split(' ')[2] == 'PM':
            if start_hour == 12:
                pass
            else: start_hour += 12
        elif start_time.split(' ')[2] == 'AM':
            if start_hour == 12:
                start_hour = 0
            else:
                start_hour

        start_minutes = int(start_time.split(' ')[1].split(':')[1].split('.')[0])
        start_seconds = int(start_time.split(' ')[1].split(':')[2].split('.')[0])
        start_date = int(start_time.split(' ')[0].split('/')[1])
        start_time_seconds = (start_date) * 24 * 60 * 60 + start_hour * 60 * 60 + start_minutes * 60 + start_seconds
        #############################################################################################################
        dt[i] = time_step(text)
        
        pos_x[i] = float(re.search(r' PosX="(\d+\.\d+)', text).group(1))
        
        if region_switch: 
            name_exp = folder_path.split(' ')[3:5]
            if i != 0:
                diff += np.abs((pos_x[i]-pos_x[i-1])*1e6) +length_x_vec[i-1]/2 - length_x_vec[i]/2
                x_dist[i] += diff

                delay[i] =  start_time_seconds - end_time_seconds
        else:
            name_exp = folder_path.split(' ')[3:5]
            if i != 0:
                diff += (pos_x[i]-pos_x[i-1])*1e6 + length_x_vec[i-1]/2 - length_x_vec[i]/2
                x_dist[i] += diff

                delay[i] =  start_time_seconds - end_time_seconds
        #############################################################################################################
        end_time = re.search(r'<EndTime>(\d{1,2}/\d{1,2}/\d{4} \d{1,2}:\d{2}:\d{2}\.\d+ [AP]M)</EndTime>',
text).group(1)
        end_hour = int(end_time.split(' ')[1].split(':')[0])
        if end_time.split(' ')[2] == 'PM':
            if end_hour == 12:
                pass
            else: end_hour += 12
        elif end_time.split(' ')[2] == 'AM':
            if end_hour == 12:
                end_hour = 0
            else:
                end_hour
        end_minutes = int(end_time.split(' ')[1].split(':')[1].split('.')[0])
        end_seconds = int(start_time.split(' ')[1].split(':')[2].split('.')[0])
        end_date = int(end_time.split(' ')[0].split('/')[1])
        end_time_seconds = (end_date) * 24 * 60 * 60 + end_hour * 60 * 60 + end_minutes * 60 + end_seconds
        #############################################################################################################

    # set the position for the scanning windows of the intensity profile
    # full x image, set the scan at half the image's height
    x_pos=1
    y_pos=2
    y = int(images[0][0].shape[0]/y_pos)
    x = int(images[0][0].shape[1]/x_pos)
    average = [[] for num in range(num_timelapse)]

    # scanning window width of 50 pixels
    l = 50
    for j in range(num_timelapse):
        for i in range(0, len(images[j])):
            slice_rows = images[j][i][y-l:y+l]
            slice_cols = slice_rows[:, 0:x]
            average[j].append(np.mean(slice_cols, axis=0))

    return average, x_dist, dt, delay

def outliers(peaks):
    '''
    Calculate the interquartile range (IQR)

    peaks (list): peaks position

    return peaks (list) with the outliers removed
    '''
    Q1 = np.percentile(peaks, 25)
    Q3 = np.percentile(peaks, 75)
    IQR = Q3 - Q1
    
    # Identify outliers as points that are more than 1.5 IQR away from the median
    lower_bound = Q1 - 1 * IQR
    upper_bound = Q3 + 1 * IQR
    
    return [x for x in peaks if lower_bound <= x <= upper_bound]

def radius_time(x_dist, average, dt, delay, t_ini = 0, r_ini = 0, prominence=0.02, mini=[False], amax=[False]):
    '''
    Calculate the radius and time from the profile data and coordinates

    x_dist (lists, float): x coordinates of timelapses
    average (lists, float): profile intensity data of timelapses
    t_ini (float): initial time
    r_ini (float): initial radius

    return: r (lists, float): radius
            t (lists, float): time
            dh (lists, float): height
            dx (lists, float): x position
            da (lists, float): angle
    '''
    delay[0] = t_ini
    r = [[] for num in range(len(x_dist))] 
    t = [[] for num in range(len(x_dist))] 
    da = [[] for num in range(len(x_dist))]
    dh = [[] for num in range(len(x_dist))]
    dx = [[] for num in range(len(x_dist))]
    for i, coord in enumerate(x_dist):
        coord = coord + r_ini
        for j, data in enumerate(average[i]):
            dy = np.where([data < np.mean(data)][-1] == True)[-1][-1]

            z = data[:dy]
            
            try:
                if amax[i]:
                    r[i].append(coord[np.argmin(data[:dy])])
                    continue
            except:
                None
            try: 
                if mini[i]: 
                    r[i].append(coord[np.argmin(data[:dy])])
                    continue
            except:
                 None
            peaks, _ = find_peaks(z, prominence=np.std(z), width=0.1)
            r[i].append(coord[outliers(peaks)[-1]])
            
            height = h(outliers(peaks))
            volume = spherical_cap_volume(r[i][-1], height[0])
            dd_vec = coord[outliers(peaks)]
            dda_vec = dd_vec[-1] - dd_vec
            angle_vec = np.arctan2(height, dda_vec)
            angle_deg_vec = np.degrees(angle_vec)
            da[i].append(angle_deg_vec)
            dh[i].append(height)
            dx[i].append(dd_vec)
        if i == 0:
            t[i] = np.linspace(t_ini, t_ini+dt[i]*j, j+1)
        else: 
            t[i] = np.linspace(t[i-1][-1]+delay[i], t[i-1][-1]+dt[i]*j+delay[i], j+1)
    return r, t, dh, dx, da