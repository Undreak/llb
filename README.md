
# **Wetting dynamics of polymer liquids from the macro to the nanometric scale**
![Silicon oil drop on silicon wafer](Image1.png)

# Dependencies
`numpy`

`matplotlib`

`pandas`

`scipy`

`skimage`

`re`

`csv`

# Data Structure
```
Folder _____
            |__ MetaData __
            |              |___ timelapse1_Properties.xml
            |              |___ timelapse2_Properties.xml
            |              |___ timelapse3_Properties.xml
            |
            |___ timelapse 1__
            |                 |___ timelapse1_Image1.tif
            |                 |___ timelapse1_Image2.tif
            |                 |___ timelapse1_Image3.tif
            |
            |___ timelapse 2__
            |                 |___ timelapse2_Image1.tif
            |                 |___ timelapse2_Image2.tif
            |                 |___ timelapse2_Image3.tif
            |
            |___ timelapse 3__
            |                 |___ timelapse3_Image1.tif
            |                 |___ timelapse3_Image2.tif
            |                 |___ timelapse3_Image3.tif
            |___ Values.csv
            |
```

`MetaData` folder contains the metadate of the images

`timelapse` folders contains the images of the timelapses

`Valuces.csv` contains the intensity values of a single image of a full droplet, used to calculate the volume of the droplet.
